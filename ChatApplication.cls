public class ChatApplication {
    @AuraEnabled
    public static List<ChatDataWrapper> getChatData() {
        List<ChatDataWrapper> chatDataList = new List<ChatDataWrapper>();
        for(Integer i=0; i<5; i++) {
            if(Math.mod(i,2) == 0) {
                chatDataList.add(new ChatDataWrapper(String.valueOf(i), 'Person 1', 'Inbound', 'Message '+(i+1)));
            }
            else {
                chatDataList.add(new ChatDataWrapper(String.valueOf(i), 'Person 2', 'Outbound', 'Message '+(i+1)));
            }
        }     
        return chatDataList;   
    }

    public class ChatDataWrapper {
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public String listItemType;
        @AuraEnabled public String messageType;
        @AuraEnabled public String messageToSend;
        
        public ChatDataWrapper(String id, String name, String listItemType, String messageToSend){
            this.id = id;
            this.name = name;
            this.messageToSend = messageToSend;
            if(listItemType == 'Inbound'){
                this.listItemType = 'slds-chat-listitem slds-chat-listitem_inbound';
                this.messageType = 'slds-chat-message__text slds-chat-message__text_inbound';
            }else{
                this.listItemType = 'slds-chat-listitem slds-chat-listitem_outbound';
                this.messageType = 'slds-chat-message__text slds-chat-message__text_outbound';
            }
        }
    }
}
