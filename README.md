# README #

### What is this repository for? ###

* It provides the structure of a basic chat application in LWC.

### How do I get set up? ###

* Add the LWC and the apex class to your org and change those according to your requirement.
* Then add this LWC to the lightning record page you want.
* It will look like below as per current LWC and apex class:

![Chat App Image](https://bitbucket.org/HimanshuShrimali/lwc-chat-application/raw/82478d4ad8d674db975608a75c920d195948797d/image/chatApp.PNG)