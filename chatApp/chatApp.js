import { LightningElement, track } from 'lwc';
import getChatData from '@salesforce/apex/ChatApplication.getChatData';

export default class ChatApp extends LightningElement {
    @track chatList = [];

    connectedCallback() {
        getChatData({})
        .then(result => {
            this.chatList = result;
            console.log(result);
        })
        .catch(error => {
            console.log('error');
        })

    }
}